# uniapp文件选择__支持小程序，安卓，文件查看

#### 介绍
uniapp文件选择--支持小程序、安卓，文件查看

#### 安装教程
1.  克隆项目
2.  拖动项目《文件选择--支持小程序、安卓，文件查看》至HbuilderX打开

#### 使用说明

1. 参考[可自定义样式的文件选择--安卓端 - DCloud 插件市场](https://ext.dcloud.net.cn/plugin?id=5337)，根据项目需要修改的样式。分享给各位有需要直接使用。

#### 截图

![QQ截图20220216174553.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhvsx5ybj30ku0jvjts.jpg)
![QQ截图20220216174611.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhvyibizj30ry0kiwi7.jpg)
![QQ截图20220216174621.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhw4q6a5j30nn0najvj.jpg)



---

有需要可加wx我：ImGeomPa或进qq群[857064044](https://jq.qq.com/?_wv=1027&k=0C7oeCWW)交流

