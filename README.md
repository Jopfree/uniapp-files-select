uniapp文件选择__支持小程序，安卓，文件查看

#### 介绍
uniapp文件选择--支持小程序、安卓，文件查看

#### 安装教程
   1.  克隆项目
   2.  拖动项目《文件选择--支持小程序、安卓，文件查看》至HbuilderX打开

#### 使用说明

参考[可自定义样式的文件选择--安卓端 - DCloud 插件市场](https://ext.dcloud.net.cn/plugin?id=5337)，根据项目需要修改的样式。分享给各位有需要直接使用。

#### 截图

   ![QQ截图20220216174553.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhvsx5ybj30ku0jvjts.jpg)
   ![QQ截图20220216174611.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhvyibizj30ry0kiwi7.jpg)
   ![QQ截图20220216174621.png](http://tva1.sinaimg.cn/large/006DFWgBgy1gzfhw4q6a5j30nn0najvj.jpg)

#### 参与贡献

   1.  Fork 本仓库
   2.  新建 Feat_xxx 分支
   3.  提交代码
   4.  新建 Pull Request

#### 特技

   1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
   2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
   3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
   4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
   5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
   6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
